<?php
namespace RestInABox\Framework\Repository\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConflictAwareRepositoryInterface
 * @package RestInABox\Framework\Repository\Contracts
 */
interface ConflictAwareRepositoryInterface extends RepositoryInterface
{
    /**
     * Ensure there is no conflict with updated model data.
     *
     * @param $model
     * @param array $attributes
     */
    public function noConflict($model, array $attributes);
}
