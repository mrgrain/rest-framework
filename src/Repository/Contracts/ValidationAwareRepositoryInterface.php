<?php
namespace RestInABox\Framework\Repository\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Interface ValidationAwareRepositoryInterface
 * @package RestInABox\Framework\Repository\Contracts
 */
interface ValidationAwareRepositoryInterface extends RepositoryInterface
{
    /**
     * Validate input data for an entity.
     * @param array $attributes
     * @param string $rules
     * @param null $id
     * @return bool
     * @throws ValidatorException
     */
    public function validate(array $attributes, $rules = ValidatorInterface::RULE_CREATE, $id = null);
}
