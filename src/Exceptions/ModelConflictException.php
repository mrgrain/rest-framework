<?php
namespace RestInABox\Framework\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;

/**
 * Class ModelConflictException
 * @package RestInABox\Framework\Exceptions
 */
class ModelConflictException extends ModelException implements MessageProvider
{
    /**
     * @param string $model
     * @param string $field
     * @param string $message
     */
    public function __construct($model, $field = 'updated_at', $message = "Resource was updated prior to your request.")
    {
        parent::__construct($message, [$field => $message], $message);
    }
}
