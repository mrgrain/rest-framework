<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fractal Presenter Config
    |--------------------------------------------------------------------------
    |

    Available serializers:
    ArraySerializer
        League\Fractal\Serializer\ArraySerializer
        League\Fractal\Serializer\DataArraySerializer
        League\Fractal\Serializer\JsonApiSerializer
        RestInABox\Framework\Serializer\NaturalSerializer

    */
    'fractal' => [
        'params' => [
            'include' => env('API_INCLUDE_PARAM', 'include')
        ],
        'serializer' => env('API_SERIALIZER', \League\Fractal\Serializer\DataArraySerializer::class)
    ],
];
